# Buguser Additions

Small and simple plugin, which adds new features like mechanics and targeters to MythicMobs!
The main focus are additions to the handling of blocks for usage in custom enchantments.

## Getting started

Just drop the newest .jar File from the [Releases](https://gitlab.com/elianobuerger/buguser-additions/-/releases) into your servers plugin folder and you're ready to make use of the new functions!

## How does it work?

Check the [Wiki](https://gitlab.com/elianobuerger/buguser-additions/-/wikis/home) :)
