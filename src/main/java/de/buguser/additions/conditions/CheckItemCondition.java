package de.buguser.additions.conditions;

import de.buguser.additions.helper.*;
import io.lumine.mythic.api.config.MythicLineConfig;
import io.lumine.mythic.api.skills.SkillCaster;
import io.lumine.mythic.api.skills.conditions.ICasterCondition;
import io.lumine.mythic.bukkit.BukkitAdapter;
import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;

import java.util.HashSet;
import java.util.regex.Pattern;

public class CheckItemCondition implements ICasterCondition {

    protected final String[] slotString;
    protected final String materialString;
    protected HashSet<Material> materials;
    protected final Range durabilityRange;
    protected final boolean durabilityToDamage;
    protected final Range amountRange;
    protected final String itemName;
    protected final String itemLore;
    protected final String hasDurability;

    public CheckItemCondition(MythicLineConfig config) {

        slotString = config.getString("slot", "HAND").toUpperCase().split(",");
        materialString = config.getString("material", "-1").toUpperCase();

        String[] materialStrings = materialString.split(",");

        materials = new HashSet<>();

        //Find all targeted materials by resolving asterisks
        for(String mat : materialStrings) {
            if(mat.contains("*")) {
                Pattern searchPattern = Pattern.compile(mat.replaceAll("\\*", ".*"));

                for(Material material : Material.values()) {
                    if(searchPattern.matcher(material.toString()).find()) {
                         materials.add(material);
                    }
                }
            }
            else {
                materials.add(Material.getMaterial(mat));
            }
        }


        String durabilityString = config.getString("durability", "-1");
        durabilityRange = new Range(durabilityString);

        durabilityToDamage = config.getBoolean("durabilityToDamage", false);

        String amountString = config.getString("amount", "-1");
        amountRange = new Range(amountString);

        itemName = config.getString("name", "-1");

        itemLore = config.getString("lore", "-1");

        hasDurability = config.getString("hasDurability", "-1");
    }


    @Override
    public boolean check(SkillCaster skillCaster) {

        if(skillCaster == null)
            return false;

        LivingEntity livingEntity = (LivingEntity) BukkitAdapter.adapt(skillCaster.getEntity());

        if(livingEntity == null || livingEntity.isDead() || livingEntity.getEquipment() == null)
            return false;

        ItemStack itemStack;
        boolean conditionsFulfilled = false;

        for(String definedSlot : slotString) {

            //find the accessed ItemStack first
            if(Utils.isNumeric(definedSlot)) {   //mobs have no inventory -> slots wouldn't work, only access to EquipmentSlots

                Player player;

                if(livingEntity instanceof Player)
                    player = (Player) livingEntity;
                else
                    continue; //can't be accessed

                int slot = Integer.parseInt(definedSlot);
                itemStack = player.getInventory().getItem(slot);
            }
            else {
                itemStack = livingEntity.getEquipment().getItem(EquipmentSlot.valueOf(definedSlot));
            }

            if(itemStack == null)
                continue;

            //Now do all the checks

            if(!checkMaterial(itemStack))
                continue;

            if(!checkAmount(itemStack))
                continue;

            if(!checkDurability(itemStack))
                continue;

            if(!checkName(itemStack))
                continue;

            if(!checkLore(itemStack))
                continue;

            if(!checkHasDurability(itemStack))
                continue;

            conditionsFulfilled = true;
        }

        return conditionsFulfilled;
    }


    private boolean checkMaterial(ItemStack itemStack) {
        if(!materialString.equalsIgnoreCase("-1")) {
            return materials.contains(itemStack.getType());
        }

        return true;
    }

    private boolean checkAmount(ItemStack itemStack) {
        if(amountRange.getMax() != -1) {

            int amount = itemStack.getAmount();

            return amountRange.contains(amount);
        }

        return true;
    }

    private boolean checkDurability(ItemStack itemStack) {
        if(durabilityRange.getMax() != -1) {

            if(!itemStack.hasItemMeta())
                return false;

            if(itemStack.getItemMeta() instanceof Damageable) {

                int itemDurability;

                if(durabilityToDamage)
                    itemDurability = ((Damageable) itemStack.getItemMeta()).getDamage();
                else
                    itemDurability = itemStack.getType().getMaxDurability() - ((Damageable) itemStack.getItemMeta()).getDamage();


                return durabilityRange.contains(itemDurability);
            }
            else {
                return false;
            }
        }

        return true;
    }

    private boolean checkName(ItemStack itemStack) {
        if(!itemName.equalsIgnoreCase("-1")) {

            if(!itemStack.hasItemMeta())
                return false;

            if(!itemStack.getItemMeta().hasDisplayName())
                return false;

            return itemStack.getItemMeta().getDisplayName().equalsIgnoreCase(itemName);
        }

        return true;
    }


    private boolean checkLore(ItemStack itemStack) {
        if(!itemLore.equalsIgnoreCase("-1")) {

            if(!itemStack.hasItemMeta())
                return false;

            if(!itemStack.getItemMeta().hasLore())
                return false;

            boolean containsLore = false;

            for(String loreLine : itemStack.getItemMeta().getLore())
            {
                if(loreLine.contains(itemLore)) {
                    containsLore = true;
                    break;
                }
            }

            return containsLore;
        }

        return true;
    }


    private boolean checkHasDurability(ItemStack itemStack) {
        if(!hasDurability.equalsIgnoreCase("-1")) {

            if(!itemStack.hasItemMeta())
                return false;

            if(hasDurability.equalsIgnoreCase("true")) {
                return itemStack.getItemMeta() instanceof Damageable;
            }
            else {
                return !(itemStack.getItemMeta() instanceof Damageable);
            }
        }

        return true;
    }

}
