package de.buguser.additions.conditions;

import de.buguser.additions.helper.Range;
import io.lumine.mythic.api.config.MythicLineConfig;
import io.lumine.mythic.api.skills.SkillCaster;
import io.lumine.mythic.api.skills.conditions.ICasterCondition;
import org.bukkit.entity.Player;

public class HungerCondition implements ICasterCondition {


    protected final Range hungerRange;

    public HungerCondition(MythicLineConfig config) {
        String hunger = config.getString("hunger", "20");
        hungerRange = new Range(hunger);
    }

    @Override
    public boolean check(SkillCaster skillCaster) {

        if(skillCaster == null)
            return false;

        Player player = (Player) skillCaster.getEntity().getBukkitEntity();

        if(player == null || player.isDead())
            return false;

        int foodLevel = player.getFoodLevel();

        return hungerRange.contains(foodLevel);
    }
}
