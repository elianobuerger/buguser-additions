package de.buguser.additions.mechanics;

import de.buguser.additions.helper.Utils;
import io.lumine.mythic.api.adapters.AbstractLocation;
import io.lumine.mythic.api.config.MythicLineConfig;
import io.lumine.mythic.api.skills.ITargetedLocationSkill;
import io.lumine.mythic.api.skills.SkillMetadata;
import io.lumine.mythic.api.skills.SkillResult;
import io.lumine.mythic.api.skills.placeholders.PlaceholderInt;
import io.lumine.mythic.bukkit.BukkitAdapter;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.FurnaceRecipe;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;

import java.util.HashSet;
import java.util.Iterator;

public class SmeltBlockMechanic implements ITargetedLocationSkill {

    protected final boolean destroyBlock;
    protected final PlaceholderInt specificAmount;
    protected final boolean respectTool;
    protected final boolean respectDurability;
    protected final boolean dropExp;


    public SmeltBlockMechanic(MythicLineConfig config) {
        respectTool = config.getBoolean("respectTool", false);
        respectDurability = config.getBoolean("respectDurability", false);
        destroyBlock = config.getBoolean("destroy", true);
        specificAmount = config.getPlaceholderInteger("amount", -1);
        dropExp = config.getBoolean("dropExp", true);
    }

    @Override
    public SkillResult castAtLocation(SkillMetadata skillMetadata, AbstractLocation abstractLocation) {

        Block block = BukkitAdapter.adapt(abstractLocation).getBlock();
        Material material = block.getType();

        Player player;
        ItemStack mainHand;

        HashSet<ItemStack> drops = new HashSet<>(block.getDrops());

        if(material == Material.AIR)
            return SkillResult.ERROR;


        if(respectTool || respectDurability)
        {
            if(skillMetadata.getCaster().getEntity().isPlayer()) {
                player = (Player) skillMetadata.getCaster().getEntity().getBukkitEntity();
                mainHand = player.getInventory().getItemInMainHand();

                if(respectDurability && mainHand.getType() != Material.AIR)
                    Utils.damageDurability(mainHand, 1);

                if(respectTool)
                    drops = new HashSet<>(block.getDrops(mainHand));
            }
        }


        for(ItemStack drop : drops) {
            smeltItemStack(drop);

            if(specificAmount.get(skillMetadata, skillMetadata.getTrigger()) >= 0)
                drop.setAmount(specificAmount.get(skillMetadata, skillMetadata.getTrigger()));

            block.getWorld().dropItemNaturally(block.getLocation(), drop);
        }

        if(dropExp && Utils.getXpFromMaterial(block.getType()) > 0)
            Utils.dropExp(block.getLocation(), Utils.getXpFromMaterial(block.getType()));

        //Drop Item
        if(destroyBlock)
            block.setType(Material.AIR);


        return SkillResult.SUCCESS;
    }



    protected static void smeltItemStack(ItemStack is) {

        String itemName = is.getType().toString();

        if(itemName.contains("LOG") || itemName.contains("WOOD")) {
            is.setType(Material.CHARCOAL);
            return;
        }

        Iterator<Recipe> recipes = Bukkit.recipeIterator();

        while(recipes.hasNext())
        {
            Recipe recipe = recipes.next();

            if(!(recipe instanceof FurnaceRecipe))
                continue;

            if(((FurnaceRecipe) recipe).getInput().getType() != is.getType())
                continue;

            is.setType(recipe.getResult().getType());
            return;
        }
    }
}
