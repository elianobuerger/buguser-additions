package de.buguser.additions.mechanics;

import de.buguser.additions.helper.Utils;
import io.lumine.mythic.api.adapters.AbstractLocation;
import io.lumine.mythic.api.config.MythicLineConfig;
import io.lumine.mythic.api.skills.ITargetedLocationSkill;
import io.lumine.mythic.api.skills.SkillMetadata;
import io.lumine.mythic.api.skills.SkillResult;
import io.lumine.mythic.bukkit.BukkitAdapter;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class DestroyBlockMechanic implements ITargetedLocationSkill {

    //Determines whether the drop should be affected by the worn tool of the player
    protected final boolean respectTool;
    //Determines if durability should be respected (lowered)
    protected final boolean respectDurability;
    //Should the destroyed block drop at all?
    protected final boolean doDrops;
    protected final boolean dropExp;

    public DestroyBlockMechanic(MythicLineConfig config) {
        respectTool = config.getBoolean("respectTool", false);
        respectDurability = config.getBoolean("respectDurability", false);
        doDrops = config.getBoolean("doDrops", true);
        dropExp = config.getBoolean("dropExp", true);
    }

    @Override
    public SkillResult castAtLocation(SkillMetadata skillMetadata, AbstractLocation abstractLocation) {

        Block block = BukkitAdapter.adapt(abstractLocation).getBlock();
        Material material = block.getType();
        Player player;
        ItemStack mainHand;


        if(material == Material.AIR)
            return SkillResult.ERROR;


        if(dropExp && Utils.getXpFromMaterial(block.getType()) > 0)
            Utils.dropExp(block.getLocation(), Utils.getXpFromMaterial(block.getType()));


        if(respectTool || respectDurability) {

            if(skillMetadata.getCaster().getEntity().isPlayer())
            {
                player = (Player) skillMetadata.getCaster().getEntity().getBukkitEntity();
                mainHand = player.getInventory().getItemInMainHand();

                if(respectDurability && mainHand.getType() != Material.AIR)
                    Utils.damageDurability(mainHand, 1);

                if(doDrops && respectTool) {
                    block.breakNaturally(mainHand);
                    return SkillResult.SUCCESS;
                }
            }
        }

        if(doDrops)
            block.breakNaturally();
        else
            block.setType(Material.AIR);

        return SkillResult.SUCCESS;
    }
}
