package de.buguser.additions.mechanics;

import de.buguser.additions.helper.Utils;
import io.lumine.mythic.api.adapters.AbstractEntity;
import io.lumine.mythic.api.config.MythicLineConfig;
import io.lumine.mythic.api.skills.ITargetedEntitySkill;
import io.lumine.mythic.api.skills.SkillMetadata;
import io.lumine.mythic.api.skills.SkillResult;
import io.lumine.mythic.api.skills.placeholders.PlaceholderInt;
import io.lumine.mythic.bukkit.BukkitAdapter;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.EquipmentSlot;

import java.util.Arrays;
import java.util.HashSet;


public class DurabilityMechanic implements ITargetedEntitySkill {

    protected final HashSet<String> armorType;
    protected final PlaceholderInt durability;

    public DurabilityMechanic(MythicLineConfig config) {
        armorType = new HashSet<>(Arrays.asList(config.getString("armor", "all").toUpperCase().split(",")));
        durability = config.getPlaceholderInteger("durability", 1);
    }

    @Override
    public SkillResult castAtEntity(SkillMetadata skillMetadata, AbstractEntity abstractEntity) {

        if(armorType == null || armorType.size() == 0)
            return SkillResult.ERROR;

        if(abstractEntity == null || abstractEntity.isDead())
            return SkillResult.CONDITION_FAILED;

        LivingEntity target = (LivingEntity) BukkitAdapter.adapt(abstractEntity);

        if(target.getEquipment() == null)
            return SkillResult.CONDITION_FAILED;

        EntityEquipment equipment = target.getEquipment();

        if(armorType.contains("all")) {
            for(EquipmentSlot eqSlot : EquipmentSlot.values())
                Utils.damageDurability(equipment.getItem(eqSlot), durability.get(skillMetadata, abstractEntity));

            return SkillResult.SUCCESS;
        }

        for(String equipmentPart : armorType) {
            EquipmentSlot eqSlot = EquipmentSlot.valueOf(equipmentPart);
            Utils.damageDurability(equipment.getItem(eqSlot), durability.get(skillMetadata, abstractEntity));
        }

        return SkillResult.SUCCESS;
    }
}
