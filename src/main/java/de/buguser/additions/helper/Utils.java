package de.buguser.additions.helper;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;

import java.util.concurrent.ThreadLocalRandom;

public class Utils {

    public static void damageDurability(ItemStack is, int damageAmount) {
        if(is.getType().getMaxDurability() == 0 || is.getType().isAir())
            return;

        Damageable damageable = (Damageable) is.getItemMeta();

        if(damageable.getDamage()+damageAmount <= is.getType().getMaxDurability()) {
            damageable.setDamage(damageable.getDamage()+damageAmount);
            is.setItemMeta(damageable);
        }
        else {
            is.setAmount(0);
        }
    }


    public static int getXpFromMaterial(Material material)
    {
        if(material == Material.COAL_ORE) {
            return ThreadLocalRandom.current().nextInt(0, 3);
        }
        if(material == Material.NETHER_GOLD_ORE) {
            return ThreadLocalRandom.current().nextInt(0, 2);
        }
        if(material == Material.DIAMOND_ORE || material == Material.EMERALD_ORE)
        {
            return ThreadLocalRandom.current().nextInt(3, 8);
        }
        if(material == Material.LAPIS_ORE || material == Material.NETHER_QUARTZ_ORE)
        {
            return ThreadLocalRandom.current().nextInt(2, 6);
        }
        if(material == Material.REDSTONE_ORE)
        {
            return ThreadLocalRandom.current().nextInt(1, 6);
        }

        return 0;
    }


    public static void dropExp(Location loc, int amount) { //not working...
        ExperienceOrb expOrb = (ExperienceOrb) loc.getWorld().spawnEntity(loc, EntityType.EXPERIENCE_ORB);
        expOrb.setExperience(amount);
    }


    public static boolean isNumeric(String numericString) {

        if(numericString == null || numericString.equals(""))
            return false;

        try {
            Integer.parseInt(numericString);
        }
        catch (NumberFormatException e) {
            return false;
        }

        return true;
    }
}
