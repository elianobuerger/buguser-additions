package de.buguser.additions.helper;

public enum Operator {
    EQUALS, SMALLER, BIGGER, SMALLER_EQUALS, BIGGER_EQUALS, BETWEEN
}
