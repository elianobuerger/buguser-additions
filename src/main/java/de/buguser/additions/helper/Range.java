package de.buguser.additions.helper;

public class Range {

    private final String intervalIdentifier = "to";
    private final int lowerBound;
    private final int upperBound;
    private final Operator operator;


    public Range(String input) {
        if(input.contains(intervalIdentifier)) { // -> we will check if another value is between those two
            String[] splitBoundaries = input.split(intervalIdentifier);

            lowerBound = Integer.parseInt(splitBoundaries[0]);
            upperBound = Integer.parseInt(splitBoundaries[1]);

            operator = Operator.BETWEEN;
        }
        else { // -> we will check if another value is smaller/bigger/equal etc.
            operator = getPrefixedOperators(input); //defaults to EQUALS if none found
            String stringValue = removePrefixedOperators(input);

            lowerBound = Integer.parseInt(stringValue);
            upperBound = lowerBound;
        }
    }


    public Range(String[] stringArray) {
        lowerBound = Integer.parseInt(stringArray[0]);
        upperBound = Integer.parseInt(stringArray[1]);
        operator = Operator.BETWEEN;
    }

    public Range(int[] intArray) {
        lowerBound = intArray[0];
        upperBound = intArray[1];
        operator = Operator.BETWEEN;
    }

    //Single value -> lowerBound=upperBound
    public Range(int value, Operator op) {
        lowerBound = value;
        upperBound = value;
        operator = op;
    }

    public boolean contains(int value) {

        switch(operator) {
            case BIGGER: return upperBound > value;
            case BIGGER_EQUALS: return upperBound >= value;
            case SMALLER: return upperBound < value;
            case SMALLER_EQUALS: return upperBound <= value;
            case EQUALS: return upperBound == value;
            case BETWEEN: return (lowerBound <= value && value <= upperBound);
        }
        return false;
    }

    public int getMin() {
        return lowerBound;
    }

    public int getMax() {
        return upperBound;
    }


    private static Operator getPrefixedOperators(String string) {
        if(string.isEmpty())
            return null;

        if(string.charAt(0) == '<') {
            if(string.length() > 1 && string.charAt(1) == '=')
                return Operator.SMALLER_EQUALS;
            else
                return Operator.SMALLER;
        }
        if(string.charAt(0) == '>') {
            if (string.length() > 1 && string.charAt(1) == '=')
                return Operator.BIGGER_EQUALS;
            else
                return Operator.BIGGER;
        }

        return Operator.EQUALS;
    }

    private static String removePrefixedOperators(String string) {
        if(string.length() > 0) {
            if(string.charAt(0) == '<') {
                if(string.length() > 1 && string.charAt(1) == '=')
                    return string.substring(2);
                else
                    return string.substring(1);
            }
            if(string.charAt(0) == '>')
                if(string.length() > 1 && string.charAt(1) == '=')
                    return string.substring(2);
                else
                    return string.substring(1);
        }

        return string;
    }


}
