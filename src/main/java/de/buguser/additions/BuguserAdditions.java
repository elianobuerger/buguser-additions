package de.buguser.additions;

import de.buguser.additions.conditions.CheckItemCondition;
import de.buguser.additions.conditions.HungerCondition;
import de.buguser.additions.mechanics.DestroyBlockMechanic;
import de.buguser.additions.mechanics.DurabilityMechanic;
import de.buguser.additions.mechanics.SmeltBlockMechanic;
import de.buguser.additions.targeter.BlockTargeter;
import de.buguser.additions.targeter.OreVeinTargeter;
import de.buguser.additions.targeter.SpecificThreatTableTarget;

import io.lumine.mythic.bukkit.events.MythicConditionLoadEvent;
import io.lumine.mythic.bukkit.events.MythicMechanicLoadEvent;
import io.lumine.mythic.bukkit.events.MythicTargeterLoadEvent;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Logger;

public final class BuguserAdditions extends JavaPlugin implements Listener {

    public static Logger logger;

    @Override
    public void onEnable() {
        logger = this.getLogger();
        Bukkit.getPluginManager().registerEvents(this, this);
        logger.info("Buguser Additions for Mythic Mobs started!");
    }

    @Override
    public void onDisable() {
        logger.info("Buguser Additions for Mythic Mobs stopped!");
    }

    @EventHandler
    public void onMythicMechanicLoad(MythicMechanicLoadEvent event) {

        if (event.getMechanicName().equalsIgnoreCase("DamageDurability")) {
            event.register(new DurabilityMechanic(event.getConfig()));
        }

        if (event.getMechanicName().equalsIgnoreCase("SmeltBlock")) {
            event.register(new SmeltBlockMechanic(event.getConfig()));
        }

        if (event.getMechanicName().equalsIgnoreCase("DestroyBlock")) {
            event.register(new DestroyBlockMechanic(event.getConfig()));
        }
    }

    @EventHandler
    public void onMythicTargeterLoad(MythicTargeterLoadEvent event) {
        if (event.getTargeterName().equalsIgnoreCase("BlockTarget")) {
            event.register(new BlockTargeter(event.getConfig()));
        }
        if (event.getTargeterName().equalsIgnoreCase("BlockOreVein")) {
            event.register(new OreVeinTargeter(event.getConfig()));
        }
        if (event.getTargeterName().equalsIgnoreCase("SpecificThreatTableTarget")) {
            event.register(new SpecificThreatTableTarget(event.getConfig()));
        }
    }

    @EventHandler
    public void onConditionsLoad(MythicConditionLoadEvent event) {
        if (event.getConditionName().equalsIgnoreCase("CheckItem")) {
            event.register(new CheckItemCondition(event.getConfig()));
        }
        if (event.getConditionName().equalsIgnoreCase("HungerCondition")) {
            event.register(new HungerCondition(event.getConfig()));
        }
    }


}
