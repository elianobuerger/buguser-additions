package de.buguser.additions.targeter;

import io.lumine.mythic.api.adapters.AbstractLocation;
import io.lumine.mythic.api.config.MythicLineConfig;
import io.lumine.mythic.api.skills.SkillMetadata;
import io.lumine.mythic.api.skills.targeters.ILocationTargeter;
import io.lumine.mythic.bukkit.BukkitAdapter;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

public class OreVeinTargeter implements ILocationTargeter {


    protected HashSet<Location> foundLocations;
    protected int searchDepth;

    public OreVeinTargeter(MythicLineConfig config) {
        searchDepth = config.getInteger("searchDepth", 5);
    }


    @Override
    public Collection<AbstractLocation> getLocations(SkillMetadata skillMetadata) {

        foundLocations = new HashSet<>();

        Player player;
        Block targetBlock = null;

        if(skillMetadata.getCaster().getEntity().isPlayer()) {
            player = (Player) skillMetadata.getCaster().getEntity().getBukkitEntity();

            targetBlock = player.getTargetBlock(100);

            if(targetBlock == null)
                return new ArrayList<>();

            if(!targetBlock.getType().toString().contains("_ORE") && targetBlock.getType() != Material.ANCIENT_DEBRIS)
                return new ArrayList<>();
        }

        if(targetBlock != null) {
            findRecursive(targetBlock, searchDepth);
        }

        if(foundLocations.isEmpty())
            return new ArrayList<>();

        ArrayList<AbstractLocation> abstractLocations = new ArrayList<>();

        for(Location loc : foundLocations) {
            abstractLocations.add(BukkitAdapter.adapt(loc));
        }

        return abstractLocations;
    }


    protected void findRecursive(Block b, int depth)
    {
        if(depth == 0)
            return;
        else
            depth--;

        if(foundLocations.contains(b.getLocation()))
            return;
        else
            foundLocations.add(b.getLocation());

        for(BlockFace blockFace : BlockFace.values())
            if(b.getRelative(blockFace).getType() == b.getType())
                findRecursive(b.getRelative(blockFace), depth);
    }
}
