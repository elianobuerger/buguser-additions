package de.buguser.additions.targeter;

import io.lumine.mythic.api.adapters.AbstractLocation;
import io.lumine.mythic.api.config.MythicLineConfig;
import io.lumine.mythic.api.skills.SkillMetadata;
import io.lumine.mythic.api.skills.targeters.ILocationTargeter;
import io.lumine.mythic.bukkit.BukkitAdapter;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collection;

public class BlockTargeter implements ILocationTargeter {

    protected final int xOffset, yOffset, zOffset;

    public BlockTargeter(MythicLineConfig config) {
        xOffset = config.getInteger("xOffset", 0);
        yOffset = config.getInteger("yOffset", 0);
        zOffset = config.getInteger("zOffset", 0);
    }

    @Override
    public Collection<AbstractLocation> getLocations(SkillMetadata skillMetadata) {

        Player player;
        Block targetBlock = null;

        if(skillMetadata.getCaster().getEntity().isPlayer()) {
            player = (Player) skillMetadata.getCaster().getEntity().getBukkitEntity();

            targetBlock = player.getTargetBlock(100);
        }

        if(targetBlock != null) {
            Location loc = targetBlock.getLocation();

            AbstractLocation absLocation = BukkitAdapter.adapt(loc.add(xOffset, yOffset, zOffset));

            ArrayList<AbstractLocation> abstractLocations = new ArrayList<>();

            abstractLocations.add(absLocation);
            
            return abstractLocations;
        }

        return new ArrayList<>();
    }
}
