package de.buguser.additions.targeter;

import io.lumine.mythic.api.adapters.AbstractEntity;
import io.lumine.mythic.api.config.MythicLineConfig;
import io.lumine.mythic.api.skills.SkillMetadata;
import io.lumine.mythic.api.skills.targeters.IEntityTargeter;
import io.lumine.mythic.core.mobs.ActiveMob;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;

public class SpecificThreatTableTarget implements IEntityTargeter {

    ArrayList<AbstractEntity> threatEntityList = new ArrayList<>();

    int position;

    public SpecificThreatTableTarget(MythicLineConfig config) {
        position = config.getInteger("position", 1);
    }

    @Override
    public Collection<AbstractEntity> getEntities(SkillMetadata skillMetadata) {

        ActiveMob activeMob = (ActiveMob) skillMetadata.getCaster();

        if(!activeMob.hasThreatTable())
            return new ArrayList<>();

        ActiveMob.ThreatTable threatTable = activeMob.getThreatTable();

        for(AbstractEntity targetEntity : threatTable.getAllThreatTargets())
        {
            if(targetEntity.isPlayer())
                threatEntityList.add(targetEntity);
        }

        if(threatEntityList.isEmpty())
            return new ArrayList<>();

        //Now sort the list
        threatEntityList.sort(new Comparator<AbstractEntity>() {
            @Override
            public int compare(AbstractEntity t1, AbstractEntity t2) {
                Double threat1 = threatTable.getThreat(t1);
                Double threat2 = threatTable.getThreat(t2);

                return threat2.compareTo(threat1);
            }

            @Override
            public boolean equals(Object o) {
                return false;
            }
        });

        ArrayList<AbstractEntity> returnList = new ArrayList<>();

        if(threatEntityList.size() < position) {
            return new ArrayList<>();
        }
        else {
            returnList.add(threatEntityList.get(position-1));
        }

        return returnList;
    }
}
